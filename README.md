This is a toy luigi example. Execute:

```
PYTHONPATH='.' luigi --module jennis_toy_example SumNumbers
```
to run the pipeline, assuming you have a working installation of Python 3 and luigi.
