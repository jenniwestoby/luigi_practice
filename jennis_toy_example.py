import luigi

"""
Luigi class that writes some numbers to numbers.txt
"""

class GenerateNumbers(luigi.Task):

    def output(self):
        return luigi.LocalTarget('numbers.txt')

    def run(self):

        #Make a dummy list of random numbers
        numbers = [12345, 23456, 34567]

        #Write numbers to file
        with self.output().open('w') as f:
            for number in numbers:
                f.write('{number}\n'.format(number=number))

"""
Luigi class that reverses numbers and write them to reversed_numbers.txt
"""

class ReverseNumbers(luigi.Task):

   def requires(self):
        return GenerateNumbers()

   def output(self):
       return luigi.LocalTarget('reversed_numbers.txt')

   def run(self):

       # read in file as list
       with self.input().open('r') as infile:
          numbers = infile.read().splitlines()

       with self.output().open('w') as outfile:
           for number in numbers:
               revnumber=number[::-1]
               outfile.write('{number}\n'.format(number=revnumber))



"""
Luigi class that sums reversed numbers and prints output
"""

#class SumNumbers
class SumNumbers(luigi.Task):

   def requires(self):
        return ReverseNumbers()

   def run(self):

       # read in file as list
       with self.input().open('r') as infile:
          numbers = infile.read().splitlines()

       total = 0

       for number in numbers:
           total = total + int(number)
           print(total)
